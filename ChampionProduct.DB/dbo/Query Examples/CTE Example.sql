﻿WITH cte (database_id, name) AS
(
select database_id, name from sys.databases
)
SELECT database_id, name
FROM cte

/*
use dme
-- 23680
-- 21070

DECLARE @FromDate	DATETIME = CAST(CONVERT(VARCHAR,DATEADD(DAY,-1,GETDATE()), 101) AS DATETIME),
		@ToDate		DATETIME = CAST(CONVERT(VARCHAR,GETDATE(), 101) AS DATETIME)



WITH result (ReceiveDate, eventId, pmsSystem, ncpdp, 
			PatientFirstName, PatientMiddleName, PatientLastName, patientDateOfBirth, patientGender, 
			PatientAddress, PatientAddress2, PatientCity, PatientState, PatientZip, 
			adminDate, Ndc, logNumber, siteOfAdministration, doseNumber)
AS (
	SELECT --top 100 --cs.Code,  n.AlternateProductCode, c.FeeServiceLineCode,
		n.ReceiveDate,
		c.ClaimId as eventId, 
		'Cardinal MedBill' as pmsSystem, s.ncpdp, 
		n.PatientFirstName, '' as PatientMiddleName, n.PatientLastName, convert(varchar,n.DateOfBirth,112) as patientDateOfBirth, 
		case n.GenderCode when 1 THEN 'M' when 2 then 'F' else 'U' end as patientGender,
		n.PatientAddress, '' as PatientAddress2, n.PatientCity, n.PatientState, n.PatientZip,
		 convert(varchar, n.FillDate,112) as adminDate,
		ISNULL(n.Ndc,'') as Ndc, '' as logNumber, '' as siteOfAdministration, 
		case when n.AlternateProductCode in ('0001A','0011A','0031A') then '1'
			when n.AlternateProductCode in ('0002A','0012A') then '2'
			when c.FeeServiceLineCode in ('0001A','0011A','0031A') then '1'
			when c.FeeServiceLineCode in ('0002A','0012A') then '2'
			else ''
		end as doseNumber
		--, *
	--select distinct N.AlternateProductCode
	FROM NcpdpClaim n WITH (NOLOCK)
	JOIN Claim c WITH (NOLOCK) ON c.NcpdpClaimId = n.NcpdpClaimId
	JOIN Store s WITH (NOLOCK) ON s.StoreId = n.StoreId AND s.IsTest = 0
	--JOIN ClaimStatus cs WITH (NOLOCK) ON cs.ClaimStatusId = n.ClaimStatusId
	WHERE n.ReceiveDate > '2021-01-01'
	and n.ReceiveDate < @ToDate
	and c.isPortalClaim = 1
	and n.PayerSequence = 1
	and (n.AlternateProductCode in ('91300','91301','91303','0001A','0002A','0011A','0012A','0031A')
	  or n.Ndc in ('59267100002','59267100003','59267100001','80777027398','80777027399','80777027310','59676052015','59676058005','59676058015'))
)
SELECT --min(ReceiveDate), count(1), 
	MAX(eventId) as eventId, pmsSystem, ncpdp, 
	PatientFirstName, PatientMiddleName, PatientLastName, patientDateOfBirth, patientGender, 
	PatientAddress, PatientAddress2, PatientCity, PatientState, PatientZip, 
	'UNK' as patientRace1, '' as patientRace2, '' as patientRace3, '' as patientRace4, '' as patientRace5, '' as patientRace6, 
	'UNK' as patientEthnicity, 
	adminDate, Ndc, logNumber, siteOfAdministration, doseNumber,
	'' as vaxExpirationDate, '' as vaxRoute, '' as cmorbidStatus, '' as seriology, 'Unknown' as occupation, 
	'unk' as isEducationWorker
FROM result --FOR JSON PATH
GROUP BY pmsSystem, ncpdp, 
	PatientFirstName, PatientMiddleName, PatientLastName, patientDateOfBirth, patientGender, 
	PatientAddress, PatientAddress2, PatientCity, PatientState, PatientZip, 
	adminDate, Ndc, logNumber, siteOfAdministration, doseNumber
HAVING MIN(ReceiveDate) >= @FromDate
*/