﻿
 

-- =============================================
-- Author:        Tabb Smith
-- Create date: 04/12/2021
-- Description:    Get All Customers
-- =============================================
-- Changed By : Matthew Bergmann
-- Change Date: 04/23/2021
-- Description: Added FirstName to order by clause
-- =============================================
CREATE PROCEDURE [dbo].[Customers_GetAll]

 

AS
BEGIN
    SET NOCOUNT ON;

 


    SELECT [CustomerId]
          ,[CustomerTypeId]
          ,[BusinessName]
          ,[LastName]
          ,[FirstName]
          ,[AddressId]
          ,[CreatedDate]
          ,[IsEnabled]
      FROM [dbo].[Customer]
     ORDER BY [LastName], [FirstName]

 

END
